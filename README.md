# A600 Accelerator Converter

Allows Amiga 600 accelerators to be tested in Amiga 500 motherboards. Removing the risk of damage to the Amiga 600.

![A600 converter board](a600pistormtester.png)

Amiga 600s use Gayle to handle communication to the CIA instead of the 6800 bus of the 68000 CPU. This board uses a CPLD to emulate this so that Amiga 600 boards will work on top of the PLCC 68000 on this board. This board can also be used just as a regular functioning PLCC 68000.

**WARNING:** V2 of this board is currently untested.

You will need KiCad 6.99 minimum to be able to open the schematics.

## BOM

| Symbol   | Component                               |
| -------- | --------------------------------------- |
| C1 - C3  | 100nF 0603                              |
| JP1, JP2 | 2pin 2.54mm male right angle pin header |
| R2 - R4  | 10K 0603                                |
| U1       | PLCC 68HC000                            |
| U3       | MAX7032S / ATF1502AS QFP44 or PLCC44    |

### Build notes

* R2 - R4 are pull up resistors so value is somewhat flexible, I ended up using 4.7K in my builds.
* Both Altera MAX7032S and Microchip ATF1502AS are compatible, MAX7064S / ATF1504AS also works but will require recompiling.
* You will need a Tag-Connect TC2030-IDC-NL to be able to flash the CPLD on the board.

## Usage

The "6800 OPTION" jumper selects which mode to run the 6800 bus in. A regular Amiga 500 68000 CPU would run with the CPU controlling the 6800 bus, for this mode the jumper should be off. With an Amiga 600, Gayle runs the 6800 bus, for this mode the jumper should be on. **Note**: to use an Amiga 600 accelerator on this board, the jumper **must** be on.

The "AMIGA BR" jumper signifies whether the CPU BR line should be hooked up to the motherboard. **Note**: to use an Amiga 600 accelerator on this board, the jumper **must** be off. If you are using this board as a regular 68000 CPU eithout an accelerator and experience instabilities then putting this jumper on might solve them.

Note the orientation of the 68000 and the "R HERE" marker for where the notch would typically be on the CPU hat. The accelerator will be rotated 180° compared to its orientation in a real Amiga 600.

## Gerbers and Compiled Firmware

Gerbers and compiled firmware along with flashing instructions can be provided for a fee. Built and flashed boards are also available, contact sales@retrosupplies.co.uk for more information.
